This is the software for the experimental setup for non-destructive testing of radio transparent samples at microwaves. The main components of the system are a vector network analyzer (VNA), one or more antennas connected to it, and a two-coordinate flat mechanical scanner that moves the sample in the vicinity of the stationary antenna. The VNA and the scanner are controlled by the personal computer, which can acquire the parameters measured by the VNA in a set of preprogrammed positions over the sample.

The software consists from two parts:
1. The firmware for a Seeeduino Mega board that controlls the two-dimensional scanner.
2. PC-side software written in Python to control the setup, acquire, and display the data.

This software has the following accompanying article:
A. Zhuravlev, V. Razevig, M. Chizh, S. Ivashov and A. Bugaev, "Non-destructive testing at microwaves using a vector network analyzer and a two-coordinate mechanical scanner," 2016 16th International Conference on Ground Penetrating Radar (GPR), Hong Kong, Hong Kong, 2016, pp. 1-5.
doi: 10.1109/ICGPR.2016.7572627
The article is available in IEEE Xplore by the following link:
http://ieeexplore.ieee.org/document/7572627/
It is also available in this repository.

Thank you!
Andrey Zhuravlev

