import visa
import msvcrt
import numpy as np
import matplotlib.pyplot as plt
from time import sleep
import struct
import time

class ZVAScan:

	def __init__(self):
		self.rm = visa.ResourceManager()
		
		self.data 		= []
		self.f1			= 20.0e9
		self.f2			= 22.5e9
		#self.nf			= 26					#the number of frequencies
		self.nf			= 2
		# the new antenna:	f1 = 20   - 22.5 GHz, nf = 26
		self.ppm		= np.array([16701, 16716])
		self.stepsize	= np.array([0.003, 0.003])
		self.sync		= np.array([0, 1])	#remove unnesessary sync after testing
		self.scanArea	= np.zeros(2)
	
	def setup_scanner(self, scanResName):
		self.scan = self.rm.open_resource(scanResName, baud_rate=76800, flow_control=visa.constants.VI_ASRL_FLOW_RTS_CTS)
	
		for i in range(0,2):
			print('Setup stepper {}: '.format(i), end='')
			print(self.scan.query('set stpr {} ppm {} stepsize {} sync {}'.format(i, self.ppm[i], self.stepsize[i], self.sync[i])), end='')
		
		
		#print('Setup stepper 0: ', end='')
		#print(self.scan.query('set stpr 0 ppm {} stepsizem {} sync {}'.format(self.ppm0, self.stepsizem0, self.sync0)), end='')
		#print('Setup stepper 1: ', end='')
		#print(self.scan.query('set stpr 1 ppm 16500 stepsizem 10'), end='')
		
	
	def setup_zva(self, zvaResName):
		
		self.zva = self.rm.open_resource(zvaResName)
		self.zva.values_format.use_binary('f', False, np.array)
		
		self.zva.write('*rst')
		self.zva.write("init1:cont off")
		self.zva.write('freq:star {:.3e}'.format(self.f1))
		self.zva.write('freq:stop {:.3e}'.format(self.f2))
		self.zva.write('SWE:POIN {}'.format(self.nf))
		self.zva.write("calc1:par:meas 'Trc1', 'S21'")
		self.zva.write('sour:pow 10')		#power 10dB
		#self.zva.write('INITiate1:IMMediate; *WAI')
		#self.zva.write("DISP:WIND:TRAC:Y:AUTO ONCE")
		self.zva.write("form real,32")
		self.zva.write("trig1:sour ext")
		#self.zva.write("init1:cont on")
		
	def adj_scan(self):
		isBusy = False
		print('Use a,s,d,w keys to move the scanner, press any key to stop when moving, Esc - exit')
		while True:
			if isBusy:
				if msvcrt.kbhit():
					kbchar = msvcrt.getch()
					self.scan.write('stop')
					ans = self.scan.read()
					print('Force stop: %s' % ans, end='')
					isBusy = False
				if self.scan.bytes_in_buffer:
					ans = self.scan.read()
					print('Stopped: %s' % ans, end='')
					isBusy = False
					
			else:
				if msvcrt.kbhit():
					kbchar = msvcrt.getch()
					if kbchar == b'a':
						print('Move left')
						self.scan.write('move stpr 1 sync 0 steps -100')
						isBusy = True
					elif kbchar == b'd':
						print('Move right')
						self.scan.write('move stpr 1 sync 0 steps 100')
						isBusy = True
					elif kbchar == b'w':
						print('Move up')
						self.scan.write('move stpr 0 sync 0 steps 100')
						isBusy = True
					elif kbchar == b's':
						print('Move down')
						self.scan.write('move stpr 0 sync 0 steps -100')
						isBusy = True
					elif kbchar == b'\x1b':
						print('Return')
						break
	
	
	#the following function implements scanning only from one side only
	#it is recommended to use when the sampling interval is small (<=0.5 cm) and/or the number of frequencies is too big (>20) 
	def scan_area2(self, size, stepsize, positioning='yes'):
		#size:			[0] - along Y, [1] - along X (meters)
		#stepsize:		[0] - along Y, [1] - along X (meters)
		#positioning:	'yes' if initial position points to the center of the target, 'no' - to scan rightaway
		
		tmrtop_scan		= 4000
		tmrtop_return	= 1500
		
		points = np.intc(np.divide(size, stepsize) + 1) #number of samples to collect
		print('points = ', end='')
		print(points)
		print('size = ', end='')
		print(size)
		print('stepsize = ', end='')
		print(stepsize)
		
		if positioning == 'yes':
			print('Positioning...')
			displacement = size/2.0
			
			self.scan.query('set stpr 0 stepsize {:.4f}'.format(displacement[0]) )
			self.scan.write('move steps -1')
			self.wait_scanner()
			
			self.scan.query('set stpr 1 stepsize {:.4f} tmrtop {}'.format(displacement[1], tmrtop_return) )
			self.scan.write('move steps 1')
			self.wait_scanner()
			self.scan.query( 'clrpul' )
			
		sleep(1)
		
		self.data = np.zeros( (self.nf, points[0], points[1]), dtype=complex)
		
		#shiftY = self.stepsize[0]*(nY-1)/2.0
		#shiftX = self.stepsize[1]*(nX-1)/2.0
		#self.scan.query( 'set stpr 0 stepsize {:.4f}'.format(shiftY) )
		#self.scan.write( 'move steps -1' )
		#self.wait_scanner()
		
		#self.scan.query( 'set stpr 1 stepsize {:.4f}'.format(shiftX) )
		#self.scan.write( 'move steps 1' )
		#self.wait_scanner()		
		self.stepsize = stepsize
		self.scan.query( 'set stpr 0 stepsize {}'.format(self.stepsize[0]) )
		self.scan.query( 'set stpr 1 stepsize {}'.format(self.stepsize[1]) )
		
		iY = 0
		#movingBack = False
		syncMissed = False
		while iY<points[0]:
		
			print('\rLines left: {}    '.format(points[0]-iY), end='')
			#if movingBack:
			#	command = 'move stpr 1 sync 1 steps {}'.format(points[1]-1)
			#else:
			command = 'move stpr 1 sync 1 steps -{} tmrtop {}'.format(points[1]-1, tmrtop_scan)
			
			self.zva.write('init1')
			sleep(0.5)
			self.scan.write(command)
			#print(sample)
			
			for iX in range(0, points[1]):
				if iX > 0:
					self.zva.write('init1')
				
				try:
					self.zva.query('*opc?')
				
				except visa.VisaIOError:
					print('The sync was missed')
					#print('Moving back...')
					#self.wait_scanner()
					syncMissed = True
					break
				
				else:
					sample = self.zva.query_values('calc1:data? sdat')
					#if movingBack:
						#self.data[:, iY, -iX-1] = sample[0::2] + 1j*sample[1::2]
					#else:
					#self.data[:, iY, iX] = sample[0::2] + 1j*sample[1::2]
					self.data[:, iY, iX] = sample[1::2] + 1j*sample[0::2] 
				#print(sample)
			
			self.wait_scanner()
			
			if syncMissed:
				print('Moving back...')
				self.scan.query( 'set stpr 1 tmrtop {}'.format(tmrtop_return) )
				self.scan.write( 'retpos' ) #this is for the last active stepper - stpr 1
				self.wait_scanner()
				self.scan.query( 'sync' )
				syncMissed = False
				
			else:
				sleep(0.5)
				self.scan.query( 'set stpr 1 tmrtop {}'.format(tmrtop_return) )
				self.scan.write('retpos')
				self.wait_scanner()
				
				iY += 1
				if iY < points[0]:
					#command = 'move stpr 0 steps 1'
					self.scan.write( 'move stpr 0 steps 1' )
					self.wait_scanner()
				
				#movingBack = not(movingBack)
		
		print('Moving to initial position')
		self.scan.query('set stpr 1 stepsize {:.4f} tmrtop {}'.format(displacement[1], tmrtop_return) )
		#if(movingBack):
			#self.scan.write('move steps 1')
			#self.wait_scanner()
		#else:
		self.scan.write('move steps -1')
		self.wait_scanner()
		
		self.scan.query('set stpr 0 stepsize {:.4f}'.format(displacement[0]) )
		self.scan.write('move steps -1')
		self.wait_scanner()
		
		print('\rScan completed!', end='')
		self.scanArea = [points[0], points[1]]
		self.stepsize = np.array(stepsize)
	
	#------------------
	#def scan_area(self, nY, nX):
	def scan_area(self, size, stepsize, positioning='yes'):
		#size:			[0] - along Y, [1] - along X (meters)
		#stepsize:		[0] - along Y, [1] - along X (meters)
		#positioning:	'yes' if initial position points to the center of the target, 'no' - to scan rightaway
		
		points = np.intc(np.divide(size, stepsize) + 1) #number of samples to collect
		print('points = ', end='')
		print(points)
		print('size = ', end='')
		print(size)
		print('stepsize = ', end='')
		print(stepsize)
		
		if positioning == 'yes':
			print('Positioning...')
			displacement = size/2.0
			
			self.scan.query('set stpr 0 stepsize {:.4f}'.format(displacement[0]) )
			self.scan.write('move steps -1')
			self.wait_scanner()
			
			self.scan.query('set stpr 1 stepsize {:.4f}'.format(displacement[1]) )
			self.scan.write('move steps 1')
			self.wait_scanner()
			
		sleep(1)
		
		self.data = np.zeros( (self.nf, points[0], points[1]), dtype=complex)
		
		#shiftY = self.stepsize[0]*(nY-1)/2.0
		#shiftX = self.stepsize[1]*(nX-1)/2.0
		#self.scan.query( 'set stpr 0 stepsize {:.4f}'.format(shiftY) )
		#self.scan.write( 'move steps -1' )
		#self.wait_scanner()
		
		#self.scan.query( 'set stpr 1 stepsize {:.4f}'.format(shiftX) )
		#self.scan.write( 'move steps 1' )
		#self.wait_scanner()		
		self.stepsize = stepsize
		self.scan.query( 'set stpr 0 stepsize {}'.format(self.stepsize[0]) )
		self.scan.query( 'set stpr 1 stepsize {}'.format(self.stepsize[1]) )
		
		iY = 0
		movingBack = False
		syncMissed = False
		while iY<points[0]:
		
			print('\rLines left: {}    '.format(points[0]-iY), end='')
			if movingBack:
				command = 'move stpr 1 sync 1 steps {}'.format(points[1]-1)
			else:
				command = 'move stpr 1 sync 1 steps -{}'.format(points[1]-1)
				
			self.zva.write('init1')
			sleep(0.5)
			self.scan.write(command)
			#print(sample)
			
			for iX in range(0, points[1]):
				if iX > 0:
					self.zva.write('init1')
				
				
				try:
					self.zva.query('*opc?')
					
				
				except visa.VisaIOError:
					print('The sync was missed')
					#print('Moving back...')
					#self.wait_scanner()
					syncMissed = True
					break
				
				else:
					sample = self.zva.query_values('calc1:data? sdat')
					if movingBack:
						self.data[:, iY, -iX-1] = sample[0::2] + 1j*sample[1::2]
					else:
						self.data[:, iY, iX] = sample[0::2] + 1j*sample[1::2]
				#print(sample)
			
			self.wait_scanner()
			
			if syncMissed:
				print('Moving back...')
				if movingBack:
					command = 'move stpr 1 sync 1 steps -{}'.format(points[1]-1)
				else:
					command = 'move stpr 1 sync 1 steps {}'.format(points[1]-1)
				self.scan.write(command)
				self.wait_scanner()
				syncMissed = False
				
			else:
				iY += 1
				if iY < points[0]:
					command = 'move stpr 0 steps 1'
					self.scan.write(command)
					self.wait_scanner()
				
				movingBack = not(movingBack)
		
		print('Moving to initial position')
		self.scan.query('set stpr 1 stepsize {:.4f}'.format(displacement[1]) )
		if(movingBack):
			self.scan.write('move steps 1')
			self.wait_scanner()
		else:
			self.scan.write('move steps -1')
			self.wait_scanner()
		self.scan.query('set stpr 0 stepsize {:.4f}'.format(displacement[0]) )
		self.scan.write('move steps -1')
		self.wait_scanner()
		
		print('\rScan completed!', end='')
		self.scanArea = [points[0], points[1]]
		self.stepsize = np.array(stepsize)
	
	def wait_scanner(self):
		while not(self.scan.bytes_in_buffer):
			sleep(0.05)
			if msvcrt.kbhit():
				kbchar = msvcrt.getch()
				self.scan.write('stop')
				return(self.scan.read())
		return self.scan.read()
	
	
	def disp(self, nf, what='real'):
		if 0 <= nf < self.nf:
			fig = plt.figure()
			
			plt.subplot(1,3,1)
			plt.imshow(self.data[nf,:,:].real, interpolation='none', cmap='gray')
			plt.title('I')
			
			plt.subplot(1,3,2)
			plt.imshow(self.data[nf,:,:].imag, interpolation='none', cmap='gray')
			plt.title('Q')
			
			plt.subplot(1,3,3)
			plt.imshow(abs(self.data[nf,:,:]), interpolation='none', cmap='gray')
			plt.title('Amp')
			
			plt.show()
		else:
			print('Wrong frequency index')
	
	
	def get_line(self, nX, dir='right'):
		self.line = np.zeros((self.nf, nX), dtype=complex)
		if dir == 'right':
			command = 'move stpr 1 sync 1 steps {}'.format(nX-1)
		elif dir == 'left':
			command = 'move stpr 1 sync 1 steps -{}'.format(nX-1)
		
		self.zva.write('init1; *wai')
		self.scan.write(command)
		
		for iX in range(0, nX):
			#self.zva.query('*opc?')
			sample = self.zva.query_values('calc1:data? sdat')
			print(sample)
			if iX < nX-1:
				self.zva.write('init1; *wai')
			self.line[:, iX] = sample[0::2] + 1j*sample[1::2]
		self.scan.read()
	
	
	
	def write(self, filename, format='multiscan'):
		
		if format=='multiscan':
			
			maxAbsVal = np.zeros(self.nf, dtype=float)
			dataNorm = np.zeros_like(self.data)
			z0 = np.zeros(self.nf, dtype=complex)
			n_elements = self.data.shape[1]*self.data.shape[2]
			
			for iFr in range(0, self.nf):
				z0[iFr]=self.data[iFr].sum()/n_elements
				dataNorm[iFr,:,:] = self.data[iFr,:,:]-z0[iFr]
				indMax = abs(dataNorm[iFr,:,:]).argmax()
				maxAbsVal[iFr] = abs(dataNorm[iFr,:,:].reshape(n_elements)[indMax])
				dataNorm[iFr,:,:] = dataNorm[iFr,:,:]/maxAbsVal[iFr]
				dataNorm[iFr,:,:] = 65535.0/2.0*dataNorm[iFr,:,:] + 65535.0/2.0 + 1j*65535.0/2.0
		
			file = open(filename, 'wb')
			file.write( struct.pack( '15s',		b'DBIxxxx36xlxxsx'))
			file.write( struct.pack( 'd',		time.mktime(time.localtime()) ) )
			file.write( struct.pack( 'I', 2) )	#the number of channels
			file.write( struct.pack( 'i', 0) )	#buffer size for annotation
			file.write( struct.pack( 'I', self.scanArea[0]) )
			file.write( struct.pack( 'I', self.scanArea[1]) )
			file.write( struct.pack( 'I', self.nf) )
			file.write( struct.pack( 'I', self.nf) )
			file.write( struct.pack( 'I', 506) )
			file.write( struct.pack( 'I', 507) )
			file.write( struct.pack( 'H', 65535) )
			file.write( struct.pack( 'H', 65535) )
			file.write( struct.pack( 'd', self.stepsize[0]*1000) ) #step size in mm along X (from left to right)
			file.write( struct.pack( 'd', self.stepsize[1]*1000) ) #step size in mm along Y (from top to bottom)
			
			for iChannel in range(0,2):
				for iFrequency in range(0,self.nf):
					for iY in range(0, self.scanArea[0]):
						for iX in range(0, self.scanArea[1]):
							if iChannel == 0:
								file.write( struct.pack('H', int(dataNorm[iFrequency, iY, iX].real) ) )
							else:
								file.write( struct.pack('H', int(dataNorm[iFrequency, iY, iX].imag) ) )
			
			frequencyGrid = np.linspace(self.f1, self.f2, self.nf)
			
			for frequency in frequencyGrid:
				file.write( struct.pack('d', frequency) )
			
			file.close()
		
		elif format=='npy':
			d = input('Enter the distance to the target in meters: ')
			d = float(d)
			file = open(filename, 'wb')
			np.save(file, self.data)
			np.save(file, self.f1)
			np.save(file, self.f2)
			np.save(file, self.nf)
			np.save(file, self.stepsize)
			np.save(file, d)
			file.close()
		
		else:
			print('Format is not supported')
	
	def read(self, filename):
		file = open(filename, 'rb')
		self.data		= np.load(file)
		self.f1			= np.load(file)
		self.f2			= np.load(file)
		self.nf			= np.load(file)
		self.stepsize	= np.load(file)
		file.close()
