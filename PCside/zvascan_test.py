import importlib
import zvascan
import numpy as np

zvascan = importlib.reload(zvascan)	#using reload because we are working on zvascan library

tb = zvascan.ZVAScan() #creating the testbed

tb.setup_scanner('COM56')
tb.setup_zva('TCPIP::192.168.0.249::INSTR')

#tb.adj_scan()

size		= np.array([0.3, 0.3])
#size = np.array([0.1, 0.1])
#stepsize	= np.array([0.01, 0.01])
stepsize = np.array([0.01333, 0.01333])

#tb.scan_area2(size, stepsize)
