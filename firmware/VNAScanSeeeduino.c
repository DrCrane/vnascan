/*
 * VNAScanSeeeduino.c
 *
 * Created: 17.06.2015 12:28:00
 *  Author: Andrey
 */ 
#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <stdio.h>
#include "xstepper.h"

#define BAUDRATE	76800
#define BUF_SIZE	128

#define UCSRA   UCSR0A
#define UCSRB   UCSR0B
#define UCSRC   UCSR0C
#define UBRRL   UBRR0L
#define UBRRH   UBRR0H
#define UDR     UDR0

#define FALSE		0
#define TRUE		1

//hardware handshake pins RTS/CTS
#define HS_PORT		PORTE
#define HS_DDR		DDRE
#define HS_CTSPIN_M	(1<<PE4)
#define HS_RTSPIN_M	(1<<PE5)
#define HS_PIN		PINE

extern xCommand commands[];
extern xStepper steppers[];

struct _linBuf {
	char*			buf;
	int				bytesTaken;
	volatile char	ready;		//ready flag to be processed
} linBuf;

int put (char c, FILE* file){
	//char is_CTS_high;
	do{ }while(HS_PIN & HS_CTSPIN_M);
	loop_until_bit_is_set(UCSRA, UDRE0);
	UDR = c;
	return 0;
}

int get (FILE* file){
	bit_clr_mask(HS_PORT, HS_RTSPIN_M);
	loop_until_bit_is_set(UCSRA, RXC0);
	bit_set_mask(HS_PORT, HS_RTSPIN_M);
	return UDR;
}

void uartInit(unsigned long baudRate){
	unsigned int _ubrr;
	
	// set baud rate
	// the formula is valid only if asynchronous double speed mode is used (U2X = 1)
	_ubrr = F_CPU/(8UL*baudRate) - 1;
	UBRRL = _ubrr;
	UBRRH = (_ubrr >> 8);

	// communication format: 8 data bits, no parity, 1 stop bit (8N1)
	UCSRA = _BV(U2X0);
	UCSRC = _BV(UCSZ00) | _BV(UCSZ01); //8 data bits
	//UCSRB = _BV(RXCIE0) | _BV(RXEN0) | _BV(TXEN0); //enable interrupts, Rx, and Tx
	UCSRB = _BV(RXEN0) | _BV(TXEN0); //Rx, and Tx
	
	//setup hardware handshake
	bit_set_mask(HS_DDR, HS_RTSPIN_M);	//output
	bit_clr_mask(HS_DDR, HS_CTSPIN_M);	//input
	bit_set_mask(HS_PORT, HS_RTSPIN_M);	//set high
}

void testTokens(char* s){
	int tokenCount=1;
	char delim[10] = " ,=\r\n\0";
	printf("\r\n\nTokens are:");
	printf("\r\nString \'%s\' starts at: %p", s, s);
	
	char* pToken = strtok(s, delim);
	printf("\r\n Token %d: \'%s\', offset: %d", tokenCount, pToken, pToken-s);
	while( (pToken = strtok(NULL, delim)) ){
		tokenCount++;
		printf("\r\n Token %d: \'%s\', offset is: %d", tokenCount, pToken, pToken-s);
	}
}

int main(void)
{
	char buf[BUF_SIZE];
	int indCmd;
	
	uartInit(BAUDRATE);
	FILE* uart = fdevopen(&put, &get);
	
	initSteppers(steppers);
		
	while(TRUE){
		sei();
		fgets(buf, BUF_SIZE, uart);
		
		indCmd = parseString2(buf);
		
		if(indCmd >= 0){
			//printf("\r\nCommand index parsed successfully!");
			//printParams();
			commands[indCmd]._function(NULL);
		}else{
			printf("ER\r\n");
		}
	}
	fclose(uart);
}

