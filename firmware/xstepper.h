#ifndef XSTEPPER_H_
#define XSTEPPER_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

//m - bit mask, not bit position!
#define bit_set_mask(p,m)	((p) |= (m))
#define bit_clr_mask(p,m)	((p) &= ~(m))
#define bit_get_mask(p,m)	((p) & (m))
#define bit_tgl_mask(p,m)	((p) ^= (m))

#define TRUE    1
#define FALSE   0
//max command name length without zero
#define MAX_COMMAND_NAME_LEN    10

enum argType {_ZERO_OR_MORE_KWARG_PAIRS, _ZERO_OR_MORE_KW};

typedef struct _xCommand {
	const char		_name[MAX_COMMAND_NAME_LEN+1];
	int				(*_function)(void*);
	enum argType	_argInfo;
} xCommand;

//stepper commands
int     cmdSet			(void*);
int     cmdGet			(void*);
int		cmdMove_pul		(void*);
int		cmdMove_steps	(void*);
int		cmdStop			(void*);
int		cmdReset_pos	(void*);
int		cmdHelp			(void*);
int		cmdParams		(void*);
int		cmdClrPul		(void*);
int		cmdResPos		(void*);
int		cmdSync			(void*);

enum paramType {_I, _UI, _LI, _UL, _F, _P};
	
#define MAX_PARAM_NAME_LEN	10

typedef struct _xParam2 {
	const char		_name[MAX_PARAM_NAME_LEN+1];
	const size_t	_addr;			//relative address from base, or absolute address
	enum paramType	_type;			//the type of the parameter
	const char		_isAddrAbs;		//TRUE if absolute address is provided
	char			_isRequested;	//parser uses this to mark if the value of this parameter listed in arguments to 'get' command
	//unsigned int	_timerTopValue; //timer top value for a timer controlling the frequency of the pulses to the steppers
} xParam2;

#define DIR_UP		1
#define DIR_DOWN	0

//This is the new stepper struct with floats rather than integers
typedef struct _xStepper {
	volatile uint8_t*	_port;
	volatile uint8_t*	_DDRport;
	uint8_t				_PUL_bm;
	uint8_t				_DIR_bm;
	uint8_t				_SYNC_bm;
	volatile uint8_t	_needToStop;
	volatile uint8_t	_isMoving;
	
	volatile uint8_t	_dir;			//remembers direction 
	
	float				_ppm;
	//int32_t			_pos_pul;
	int32_t				_steps_tgo;
	float				_step_m;
	int32_t				_pul_tgo;
	int					_sync;
	unsigned int		_timerTopValue; //timer top value for a timer controlling the frequency of the pulses to the steppers
	
	volatile int32_t	_pulsesMadeAbs; //counts the absolute issued number of pulses to return to exactly the same position no matter how many steps were done
	
	
	
	//xCommand*			_commands;
	//xParam*			_params;
} xStepper;

/*
typedef struct _xStepper {
	volatile uint8_t*	_port;
	volatile uint8_t*	_DDRport;
	uint8_t				_PUL_bm;
	uint8_t				_DIR_bm;
	uint8_t				_SYNC_bm;
	volatile uint8_t	_needToStop;
	volatile uint8_t	_isMoving;
	
	int32_t				_ppm;
	int32_t				_pos_pul;
	int32_t				_steps_tgo;
	int32_t				_step_mm;
	int32_t				_pul_tgo;
	int					_sync;
	
	//xCommand*			_commands;
	//xParam*			_params;
} xStepper;
*/

//parser commands and facilities
int     parseString(char*);
int		parseString2(char*);
void    printCommands(void);
void    printParams(void);
void	printParamValue(int);
//void	printParamName(int);
//void    testTokens(char*, const char*);
int     findCommandIndex(const xCommand*, const char* token);
int     findParamIndex(const xParam2* params, const char* token);
int     readParamValue(xParam2* params, int parIndex, const char* token);

//stepper functions
void	initSteppers(xStepper* steppers);

ISR(TIMER1_COMPA_vect);

#endif /* XSTEPPER_H_ */