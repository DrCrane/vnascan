#include "xstepper.h"
#include <stddef.h>
#include <math.h>

#define MAX_COMMANDS	9
xCommand commands[MAX_COMMANDS] = {
	{._name="set\0",		._function=&cmdSet,			._argInfo=_ZERO_OR_MORE_KWARG_PAIRS},
	{._name="get\0",		._function=&cmdGet,			._argInfo=_ZERO_OR_MORE_KW},
	{._name="move\0",		._function=&cmdMove_steps,	._argInfo=_ZERO_OR_MORE_KWARG_PAIRS},
	{._name="stop\0",		._function=&cmdStop,		._argInfo=_ZERO_OR_MORE_KW},
	{._name="help\0",		._function=&cmdHelp,		._argInfo=_ZERO_OR_MORE_KW},
	{._name="params\0",		._function=&cmdParams,		._argInfo=_ZERO_OR_MORE_KW},
	{._name="clrpul\0",		._function=&cmdClrPul,		._argInfo=_ZERO_OR_MORE_KW},
	{._name="retpos\0",		._function=&cmdResPos,		._argInfo=_ZERO_OR_MORE_KW},
	{._name="sync\0",		._function=&cmdSync,		._argInfo=_ZERO_OR_MORE_KW},

};

volatile int act_stpr = 0;
#define CMD4INT_NORMAL	0
#define CMD4INT_RETPOS	1
#define CMD4INT_SYNC	2
//cmd4Int is used to signal interrupt routine mode of operation
volatile uint8_t cmd4Int = CMD4INT_NORMAL;
#define MAX_PARAMS_2	7

//This is new parameters with floats rather than integers
xParam2 params2[MAX_PARAMS_2] = {
	{._name="stpr\0",		._addr=(size_t) &act_stpr,					._type=_I,		._isAddrAbs=TRUE,	._isRequested=FALSE},	//the active stepper number
	{._name="ppm\0",		._addr=offsetof(xStepper, _ppm),			._type=_F,		._isAddrAbs=FALSE,	._isRequested=FALSE},	//pulses per meter
	{._name="steps\0",		._addr=offsetof(xStepper, _steps_tgo),		._type=_LI,		._isAddrAbs=FALSE,	._isRequested=FALSE},	//number of steps to go
	{._name="stepsize\0",	._addr=offsetof(xStepper, _step_m),			._type=_F,		._isAddrAbs=FALSE,	._isRequested=FALSE},	//step size in millimeters
	{._name="sync\0",		._addr=offsetof(xStepper, _sync),			._type=_I,		._isAddrAbs=FALSE,	._isRequested=FALSE},	//set to one if sync signal is needed
	{._name="tmrtop\0",		._addr=offsetof(xStepper, _timerTopValue),	._type=_UI,		._isAddrAbs=FALSE,	._isRequested=FALSE},	//timer top value defines the speed of the stepper
	{._name="pulabs\0",		._addr=offsetof(xStepper, _pulsesMadeAbs),	._type=_LI,		._isAddrAbs=FALSE,	._isRequested=FALSE},	//pulmade counts the total number of pulses given to a stepper no matter how many discrete steps were made on a scan line
};

#define NUM_OF_STEPPERS	3
#define TIMER_TOP_VALUE 4000
//This is the new initialization with floats rather than integers
xStepper steppers[NUM_OF_STEPPERS]={
	{._port=&PORTA, ._DDRport=&DDRA, ._PUL_bm=_BV(PA0), ._DIR_bm=_BV(PA1), ._SYNC_bm=_BV(PA4), ._needToStop=FALSE, ._isMoving=FALSE, ._ppm=1.0, ._steps_tgo=0, ._step_m=1.0, ._pul_tgo=0, ._sync=FALSE, ._timerTopValue=TIMER_TOP_VALUE, ._pulsesMadeAbs=0, ._dir=DIR_DOWN},
	{._port=&PORTA, ._DDRport=&DDRA, ._PUL_bm=_BV(PA2), ._DIR_bm=_BV(PA3), ._SYNC_bm=_BV(PA4), ._needToStop=FALSE, ._isMoving=FALSE, ._ppm=1.0, ._steps_tgo=0, ._step_m=1.0, ._pul_tgo=0, ._sync=FALSE, ._timerTopValue=TIMER_TOP_VALUE, ._pulsesMadeAbs=0, ._dir=DIR_DOWN},
	{._port=&PORTA, ._DDRport=&DDRA, ._PUL_bm=_BV(PA5), ._DIR_bm=_BV(PA6), ._SYNC_bm=_BV(PA4), ._needToStop=FALSE, ._isMoving=FALSE, ._ppm=1.0, ._steps_tgo=0, ._step_m=1.0, ._pul_tgo=0, ._sync=FALSE, ._timerTopValue=TIMER_TOP_VALUE, ._pulsesMadeAbs=0, ._dir=DIR_DOWN},
};

/*
xStepper steppers[NUM_OF_STEPPERS]={
	{._port=&PORTA, ._DDRport=&DDRA, ._PUL_bm=_BV(PA0), ._DIR_bm=_BV(PA1), ._SYNC_bm=_BV(PA4), ._needToStop=FALSE, ._isMoving=FALSE, ._ppm=1, ._pos_pul=0, ._steps_tgo=0, ._step_mm=1, ._pul_tgo=0, ._sync=FALSE},
	{._port=&PORTA, ._DDRport=&DDRA, ._PUL_bm=_BV(PA2), ._DIR_bm=_BV(PA3), ._SYNC_bm=_BV(PA4), ._needToStop=FALSE, ._isMoving=FALSE, ._ppm=1, ._pos_pul=0, ._steps_tgo=0, ._step_mm=1, ._pul_tgo=0, ._sync=FALSE},
	{._port=&PORTA, ._DDRport=&DDRA, ._PUL_bm=_BV(PA5), ._DIR_bm=_BV(PA6), ._SYNC_bm=_BV(PA4), ._needToStop=FALSE, ._isMoving=FALSE, ._ppm=1, ._pos_pul=0, ._steps_tgo=0, ._step_mm=1, ._pul_tgo=0, ._sync=FALSE},
};
*/

//xStepper* activeStepper;

//----------Commands start here -------------
int cmdSet(void* ptr){
	printf("OK\r\n");
	return 0;
}

int cmdGet(void* ptr){
	for (int parInd=0; parInd<MAX_PARAMS_2; parInd++){
		if(params2[parInd]._isRequested == TRUE){
			printParamValue(parInd);
			if(parInd < MAX_PARAMS_2-1) printf(" ");
			params2[parInd]._isRequested = FALSE;
		}
			
	}
	printf("\r\n");
	return 0;
}

int	cmdMove_steps(void* ptr){
	//printf("\r\n->cmdMove_steps");
	if(steppers[act_stpr]._steps_tgo > 0){
		//set DIR bit
		bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._DIR_bm);
		steppers[act_stpr]._dir = DIR_UP;
	}else{
		//clear DIR bit
		bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._DIR_bm);
		steppers[act_stpr]._steps_tgo = -steppers[act_stpr]._steps_tgo;
		steppers[act_stpr]._dir = DIR_DOWN;
	}
	steppers[act_stpr]._isMoving = TRUE;
	TCNT1 = 0;
	OCR1A = steppers[act_stpr]._timerTopValue;
	bit_set_mask(TIMSK1, _BV(OCIE1A) ); //enable OC interrupt for A
	bit_set_mask(TCCR1B, _BV(CS11));	//enable clock 1/8 from prescaler
	sei();
	return 0;
}

int cmdStop(void* ptr){
	//printf("\r\n->cmdStop");
	if(steppers[act_stpr]._isMoving){
		steppers[act_stpr]._needToStop = TRUE;
		while(steppers[act_stpr]._isMoving);
	}
	//printf("OK\r\n");
	return 0;
}

int cmdHelp(void* ptr){
	printCommands();
	return 0;
}

int cmdParams(void* ptr){
	printParams();
	return 0;
}

int cmdClrPul(void* ptr){
	steppers[act_stpr]._pulsesMadeAbs = 0;
	printf("OK\r\n");
	return 0;
}

int cmdResPos(void* ptr){
	if(steppers[act_stpr]._pulsesMadeAbs > 0){
		//clr DIR bit
		bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._DIR_bm);
		//steppers[act_stpr]._dir = DIR_UP;
	}else{
		//clear DIR bit
		bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._DIR_bm);
		//steppers[act_stpr]._steps_tgo = -steppers[act_stpr]._steps_tgo;
		//steppers[act_stpr]._dir = DIR_DOWN;
	}
	cmd4Int = CMD4INT_RETPOS;
	steppers[act_stpr]._isMoving = TRUE;
	TCNT1 = 0;
	OCR1A = steppers[act_stpr]._timerTopValue;
	bit_set_mask(TIMSK1, _BV(OCIE1A) ); //enable OC interrupt for A
	bit_set_mask(TCCR1B, _BV(CS11));	//enable clock 1/8 from prescaler
	sei();
	return 0;
	//printf("OK\r\n");
	//return 0;
}

int cmdSync(void* ptr){
	cmd4Int = CMD4INT_SYNC;
	TCNT1 = 0;
	OCR1A = 4000;
	bit_set_mask(TIMSK1, _BV(OCIE1A) ); //enable OC interrupt for A
	bit_set_mask(TCCR1B, _BV(CS11));	//enable clock 1/8 from prescaler
	sei();
	return 0;
}



	
int parseString(char* str){
	//get first token - command name
	const char delimiter[7] = " ,=\r\n\0";
	
	if(!strlen(str)){
		//printf("\r\n String zero");
		return -1;
	}
	
	char* pToken = strtok(str, delimiter);

	int indCmd;
	
	//printf("\r\n Token: %s", pToken);
	if( (indCmd = findCommandIndex(commands, pToken)) < 0 ){
		return -1;
	}
	//printf("\r\n Command index: %d", indCmd);
	//parse arguments here
	while( (pToken = strtok(NULL, delimiter)) ){
		int parIndex;
		//printf("\r\n Token: %s", pToken);
		if( (parIndex=findParamIndex(params2, pToken)) < 0 ){
			//printf("\r\n Bad param index");
			return -1;
		}
		//printf("\r\n Param index: %d", parIndex);
		if ( (pToken = strtok(NULL, delimiter)) ){
			//parse parameter value
			if(readParamValue(params2, parIndex, pToken) < 0){
				//printf("\r\n Bad param value");
				return -1;
			}
		}else{
			//no value provided, return error
			return -1;
		}
	}
	return indCmd;
}

enum parseState {_PARSE_CMD_NAME, _PARSE_KW, _PARSE_ARG};

int parseString2(char* str){
		//get first token - command name
		const char delimiter[7] = " ,=\r\n\0";
		int state=_PARSE_CMD_NAME;
		
		if(!strlen(str)){
			//printf("\r\n String zero");
			return -1;
		}
		
		int		indCmd;		//command index
		int		parIndex;	//parameter index
		char*	pToken;		//pointer to token
		
		while(TRUE){
			switch(state){
				case _PARSE_CMD_NAME:
					pToken = strtok(str, delimiter);
					if( (indCmd = findCommandIndex(commands, pToken)) < 0 ){
						return -1;
					}else{
						state = _PARSE_KW;
					}
					break;
					
				case _PARSE_KW:
					if( (pToken = strtok(NULL, delimiter)) ){
						if( (parIndex=findParamIndex(params2, pToken)) < 0 ){
							return -1; //wrong kw provided
						}else{
							if(commands[indCmd]._argInfo == _ZERO_OR_MORE_KWARG_PAIRS){
								state = _PARSE_ARG;
							}else{
								params2[parIndex]._isRequested = TRUE;
								break;
							}
						}
					}else{
						//no kw provided
						return indCmd;
					}
					break;
				
				case _PARSE_ARG:
					if ( (pToken = strtok(NULL, delimiter)) ){
						if(readParamValue(params2, parIndex, pToken) < 0){
							//bad param value
							return -1;
						}else{
							state = _PARSE_KW;
							break;
						}
					}else{
						//arg was not provided. error
						return -1;
					}
					break;
			}
		}
}

/*
		char* pToken = strtok(str, delimiter);

		int indCmd;
		
		//printf("\r\n Token: %s", pToken);
		if( (indCmd = findCommandIndex(commands, pToken)) < 0 ){
			return -1;
		}
		//printf("\r\n Command index: %d", indCmd);
		//parse arguments here
		while( (pToken = strtok(NULL, delimiter)) ){
			int parIndex;
			//printf("\r\n Token: %s", pToken);
			if( (parIndex=findParamIndex(params2, pToken)) < 0 ){
				//printf("\r\n Bad param index");
				return -1;
			}
			//printf("\r\n Param index: %d", parIndex);
			if ( (pToken = strtok(NULL, delimiter)) ){
				//parse parameter value
				if(readParamValue(params2, parIndex, pToken) < 0){
					//printf("\r\n Bad param value");
					return -1;
				}
			}else{
				//no value provided, return error
				return -1;
			}
		}
		return indCmd;
}
*/

int findCommandIndex(const xCommand* commands, const char* token){
	int cmdIndex = -1;
	for (int i=0; i<MAX_COMMANDS; i++){
		//printf("\r\n Check command: %s", commands[i]._name);
		if ( !strcmp(commands[i]._name, token) ){
			//printf(" yes!");
			cmdIndex = i;
			break;
		}
	}
	return cmdIndex;
}

int findParamIndex(const xParam2* params, const char* token){
	int parIndex = -1;
	for (int i=0; i<MAX_PARAMS_2; i++){
		if ( !strcmp(params[i]._name, token) ){
			parIndex = i;
			break;
		}
	}
	return parIndex;
}

int readParamValue(xParam2* params, int parIndex, const char* token){
	//{_I, _UI, _LI, _UL, _F, _P}
	int result = -1;
	//char format[4];
	switch(params[parIndex]._type){
		case _I:
			if(params[parIndex]._isAddrAbs){
				if( sscanf(token, "%d", (int*)params[parIndex]._addr) )
				result = 0;
			}else{
				if( sscanf(token, "%d", (int*)(((char*)(steppers+act_stpr))+params[parIndex]._addr)) )
				result = 0;
			}
			break;
			
		case _UI:			
			if(params[parIndex]._isAddrAbs){
				if( sscanf(token, "%u", (unsigned int*)params[parIndex]._addr) )
				result = 0;
			}else{
				if( sscanf(token, "%u", (unsigned int*)(((char*)(steppers+act_stpr))+params[parIndex]._addr)) )
				result = 0;
			}
			break;
			
		case _LI:
			if(params[parIndex]._isAddrAbs){
				if( sscanf(token, "%ld", (long int*)params[parIndex]._addr) )
					result = 0;
			}else{
				if( sscanf(token, "%ld", (long int*)(((char*)(steppers+act_stpr))+params[parIndex]._addr)) )
					result = 0;
			}
			break;
			
		case _F:
			if(params[parIndex]._isAddrAbs){
				if( sscanf(token, "%f", (float*)params[parIndex]._addr) )
					result = 0;
			}else{
				if( sscanf(token, "%f", (float*)(((char*)(steppers+act_stpr))+params[parIndex]._addr)) )
					result = 0;
			}
			break;
			
		case _P:
			//special case to be handled appropriately
			result = -1;
			break;
			
		default:
		result = -1;
	}
	
	return result;
}

void printCommands(void){
	//printf("\r\nCommand names:");
	for(int i = 0; i<MAX_COMMANDS; i++){
		printf("%s ", commands[i]._name);
	}
	printf("\r\n");
}

void printParams(void){
	//printf("\r\nParameters for stepper %d: ", act_stpr);
	for(int parIndex=0; parIndex<MAX_PARAMS_2; parIndex++){
		printf("%s: ", params2[parIndex]._name);
		switch(params2[parIndex]._type){
			case _I:
				if(params2[parIndex]._isAddrAbs){
					printf("%d ", *(int*)params2[parIndex]._addr);
				}else{
					printf("%d ", *(int*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
				}
			break;
			case _UI:
				if(params2[parIndex]._isAddrAbs){
					printf("%u ", *(int*)params2[parIndex]._addr);
				}else{
					printf("%u ", *(unsigned int*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
				}
			break;
			
			case _LI:
				if(params2[parIndex]._isAddrAbs){
					printf("%ld ", *(long int*)params2[parIndex]._addr);
				}else{
					printf("%ld ", *(long int*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
				}
				break;
			
			case _F:
				if(params2[parIndex]._isAddrAbs){
					printf("%.4f ", *(float*)params2[parIndex]._addr);
				}else{
					printf("%.4f ", *(float*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
				}
				break;
			
			case _P:
				//special case to be handled appropriately
				printf("ERR ");
			break;
			
			default:
				printf("ERR ");
		}
	}
	printf("\r\n");
}

void printParamValue(int parIndex){
	switch(params2[parIndex]._type){
		case _I:
			if(params2[parIndex]._isAddrAbs){
				printf("%d", *(int*)params2[parIndex]._addr);
			}else{
				printf("%d", *(int*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
			}
			break;
		
		case _UI:
			if(params2[parIndex]._isAddrAbs){
				printf("%u", *(int*)params2[parIndex]._addr);
			}else{
				printf("%u", *(unsigned int*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
			}
			break;
				
		case _LI:
			if(params2[parIndex]._isAddrAbs){
				printf("%ld", *(long int*)params2[parIndex]._addr);
			}else{
				printf("%ld", *(long int*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
			}
			break;
				
		case _F:
			if(params2[parIndex]._isAddrAbs){
				printf("%.4f", *(float*)params2[parIndex]._addr);
			}else{
				printf("%.4f", *(float*)((char*)(steppers+act_stpr)+params2[parIndex]._addr));
			}
			break;
			
		case _P:
			//special case to be handled appropriately
			printf("ERR");
			break;
			
			default:
			printf("ERR");
	}
	return;
}


void initSteppers(xStepper* st) {
	//#define OCR1A_REG_VALUE 4000
		
	for(int i=0; i<NUM_OF_STEPPERS; i++){
		*st[i]._DDRport |= st[i]._PUL_bm | st[i]._DIR_bm | st[i]._SYNC_bm;
		*st[i]._port	&= ~(st[i]._PUL_bm | st[i]._DIR_bm | st[i]._SYNC_bm);
	}
	
	//TOP value of the timer
	//OCR1A = OCR1A_REG_VALUE;
	
	//setup timer registers here
	bit_set_mask(TCCR1B, _BV(WGM12) );
	
	//enable global interrupt
	//sei();
	return;
}

//this is the new control ISR using floats instead of integers
ISR(TIMER1_COMPA_vect){
	//static uint8_t intCount=0; //counts interrupts (toggles) from the start
	//static uint32_t stepsLeftToSync = 0;
	#define DELAY_CYCLES_BEFORE_1ST_SYNC	10
	#define DELAY_CYCLES_AFTER_1ST_SYNC		10
	static int32_t	pulsesMade		= 0;		//counts pulses given to a stepper
	static float	distance		= 0;		//stores traveled distance
	static int32_t	currentStep		= 1;		//stores current step in progress
	//static int32_t pulsesLeft = 0;  //pulses left to make a full step
	static int32_t delayCyclesBefore1stSync = DELAY_CYCLES_BEFORE_1ST_SYNC;
	static int32_t delayCyclesAfter1stSync	= DELAY_CYCLES_AFTER_1ST_SYNC;
	//enum color_t { RED, GREEN, BLUE} r = RED;
	//static enum {PUL_SYNC_RISE, PUL_SYNC_DROP, PUL_RISE, PUL_DROP} _state = PUL_SYNC_RISE;
	static enum {DELAY_BEFORE_1ST_SYNC, FIRST_SYNC_DROP, DELAY_AFTER_1ST_SYNC, PUL_RISE, PUL_DROP, LAST_SYNC_DROP, RETPOS_PUL_RISE, RETPOS_PUL_DROP, JUST_SYNC_RISE, JUST_SYNC_DROP} _state = DELAY_BEFORE_1ST_SYNC;
	
	if (cmd4Int == CMD4INT_RETPOS){
		_state = RETPOS_PUL_RISE;
		cmd4Int = CMD4INT_NORMAL;
	}else if(cmd4Int == CMD4INT_SYNC){
		_state = JUST_SYNC_RISE;
		cmd4Int = CMD4INT_NORMAL;
	}
		
	
	switch(_state){
		case DELAY_BEFORE_1ST_SYNC:
			if(steppers[act_stpr]._sync){
				if(delayCyclesBefore1stSync-- <= 0){
					//rise sync
					bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
					_state = FIRST_SYNC_DROP;
					delayCyclesBefore1stSync = DELAY_CYCLES_BEFORE_1ST_SYNC;
				}
			}else{
				_state = PUL_RISE;
			}
			break;
		
		case FIRST_SYNC_DROP:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			_state = DELAY_AFTER_1ST_SYNC;
			break;
		
		case DELAY_AFTER_1ST_SYNC:
			if(delayCyclesAfter1stSync-- <= 0){
				_state = PUL_RISE;
				delayCyclesAfter1stSync = DELAY_CYCLES_AFTER_1ST_SYNC;
			}
			break;
		
		case PUL_RISE:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);		//drop sync if raised
			if(steppers[act_stpr]._needToStop){
				bit_clr_mask(TIMSK1, _BV(OCIE1A) );										//clear interrupt
				bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));					//set no clock source
				steppers[act_stpr]._needToStop	= FALSE;
				steppers[act_stpr]._isMoving	= FALSE;
				_state = DELAY_BEFORE_1ST_SYNC;
				pulsesMade	= 0;
				distance	= 0;
				currentStep	= 1;
				printf("OK\r\n");
				break;
			}
			bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
			_state = PUL_DROP;
			break;
		
		case PUL_DROP:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
			pulsesMade++;
			
			if(steppers[act_stpr]._dir == DIR_UP)
				steppers[act_stpr]._pulsesMadeAbs++;
			else
				steppers[act_stpr]._pulsesMadeAbs--;
				
			distance = pulsesMade/steppers[act_stpr]._ppm;
			if(distance >= currentStep*steppers[act_stpr]._step_m){
				//raise sync here if required
				if(steppers[act_stpr]._sync)
					bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
				if (currentStep >= steppers[act_stpr]._steps_tgo){
					_state = LAST_SYNC_DROP;
				}else{
					_state = PUL_RISE;
					currentStep++;
				}
			}else{
				_state = PUL_RISE;
			}
			break;
		
		case LAST_SYNC_DROP:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			bit_clr_mask(TIMSK1, _BV(OCIE1A) );								//clear interrupt
			bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));			//set no clock source
			steppers[act_stpr]._isMoving	= FALSE;
			_state = DELAY_BEFORE_1ST_SYNC;
			pulsesMade	= 0;
			distance	= 0;
			currentStep	= 1;
			printf("OK\r\n");
			break;
			
		case RETPOS_PUL_RISE:
			if(steppers[act_stpr]._needToStop){
				bit_clr_mask(TIMSK1, _BV(OCIE1A) );										//clear interrupt
				bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));					//set no clock source
				steppers[act_stpr]._needToStop	= FALSE;
				steppers[act_stpr]._isMoving	= FALSE;
				_state = DELAY_BEFORE_1ST_SYNC;
				//pulsesMade	= 0;
				//distance	= 0;
				//currentStep	= 1;
				printf("OK\r\n");
				break;
			}
			bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
			_state = RETPOS_PUL_DROP;
			break;
		
		case RETPOS_PUL_DROP:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
			if(steppers[act_stpr]._pulsesMadeAbs > 0){
				steppers[act_stpr]._pulsesMadeAbs--;
				_state = RETPOS_PUL_RISE;
			}else if(steppers[act_stpr]._pulsesMadeAbs < 0){
				steppers[act_stpr]._pulsesMadeAbs++;
				_state = RETPOS_PUL_RISE;
			}
			if(steppers[act_stpr]._pulsesMadeAbs == 0){
				bit_clr_mask(TIMSK1, _BV(OCIE1A) );								//clear interrupt
				bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));			//set no clock source
				steppers[act_stpr]._isMoving	= FALSE;
				_state = DELAY_BEFORE_1ST_SYNC;
				printf("OK\r\n");
			}
			break;
		
		case JUST_SYNC_RISE:
			bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			_state = JUST_SYNC_DROP;
			break;
		
		case JUST_SYNC_DROP:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			bit_clr_mask(TIMSK1, _BV(OCIE1A) );									//clear interrupt
			bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));				//set no clock source
			_state = DELAY_BEFORE_1ST_SYNC;
			printf("OK\r\n");
			break;
		
	};
	
	return;
}

/*
ISR(TIMER1_COMPA_vect){
	//static uint8_t intCount=0; //counts interrupts (toggles) from the start
	//static uint32_t stepsLeftToSync = 0;
	#define NUMBER_OF_DELAY_CYCLES 10
	static int32_t pulsesLeft = 0;  //pulses left to make a full step
	static int32_t delayCycles = NUMBER_OF_DELAY_CYCLES;
	//enum color_t { RED, GREEN, BLUE} r = RED;
	//static enum {PUL_SYNC_RISE, PUL_SYNC_DROP, PUL_RISE, PUL_DROP} _state = PUL_SYNC_RISE;
	static enum {DELAY, SYNC_RISE, SYNC_DROP, PUL_RISE, PUL_DROP, LAST_SYNC} _state = DELAY;
	switch(_state){
		case DELAY:
			if(delayCycles-- == 0){
				_state = SYNC_RISE;
				delayCycles = NUMBER_OF_DELAY_CYCLES;
			}
			break;
			
		case SYNC_RISE:
			if(steppers[act_stpr]._sync)
				bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			//printf(".");
			_state = SYNC_DROP;
			break;
		
		case SYNC_DROP:
			if(steppers[act_stpr]._sync)
				bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			pulsesLeft = (int32_t) (0.001*steppers[act_stpr]._ppm*steppers[act_stpr]._step_mm);
			if(pulsesLeft <= 0){
				//stop interrupt here, reset to original state
				bit_clr_mask(TIMSK1, _BV(OCIE1A) );								//clear interrupt
				bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));			//set no clock source
				steppers[act_stpr]._isMoving	= FALSE;
				_state = SYNC_RISE;
			}else{
				bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
				_state = PUL_DROP;
			}
			break;
		
		case PUL_RISE:
			bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
			_state = PUL_DROP;
			break;
		
		case PUL_DROP:
			bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._PUL_bm);
			if(steppers[act_stpr]._needToStop){
				bit_clr_mask(TIMSK1, _BV(OCIE1A) );										//clear interrupt
				bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));					//set no clock source
				steppers[act_stpr]._needToStop	= FALSE;
				steppers[act_stpr]._isMoving	= FALSE;
				_state = SYNC_RISE;
				printf("OK\r\n");
				break;
			}
			pulsesLeft--;
			if(pulsesLeft <= 0){
				if(steppers[act_stpr]._steps_tgo < 0) {steppers[act_stpr]._steps_tgo++;}
				if(steppers[act_stpr]._steps_tgo > 0) {steppers[act_stpr]._steps_tgo--;}
				if(steppers[act_stpr]._steps_tgo == 0){
					if(steppers[act_stpr]._sync)
						bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
					//printf(".");
					_state = LAST_SYNC;
					break;
				}else{
					if(steppers[act_stpr]._sync)
						bit_set_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
					//printf(".");
					_state = SYNC_DROP;
					break;
				}
			}else{
				_state = PUL_RISE;
				break;
			}
			break;
			
		case LAST_SYNC:
			if(steppers[act_stpr]._sync)
				bit_clr_mask(*steppers[act_stpr]._port, steppers[act_stpr]._SYNC_bm);
			bit_clr_mask(TIMSK1, _BV(OCIE1A) );								//clear interrupt
			bit_clr_mask(TCCR1B, _BV(CS10)|_BV(CS11)|_BV(CS12));			//set no clock source
			steppers[act_stpr]._isMoving	= FALSE;
			//_state = SYNC_RISE;
			_state = DELAY;
			printf("OK\r\n");
			break;
		
	};
	
	return;
}
*/
//interrupt timer handler place here